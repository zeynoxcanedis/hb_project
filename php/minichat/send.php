<?php


$bdd = new PDO('mysql:host=localhost;dbname=minichat', "root", "");

$sql = $bdd->query('SELECT id FROM utilisateur WHERE pseudo=:pseudo and mot_de_passe=:mdp)');

$msg = $bdd->prepare('INSERT INTO message(contenu, id_utilisateur, date_envoi) VALUES(:message, :id, :datevenvoi)');
session_start();
$sql->execute(array(
    'pseudo' => $_SESSION['id'],
    'mdp' => $_SESSION['mdp'],
));
$donnee = $sql->fetchAll();
$msg->execute(array(
    'message' => $_POST['message'],
    'id' => $donnee,
    'dateenvoie' => date('Y-m-d H:i:s'),
));

header('location: chat.php');

exit;