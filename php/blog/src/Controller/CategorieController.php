<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="category")
     * @param CategoryRepository $categoryRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CategoryRepository $categoryRepository)
    {
        $categories = $categoryRepository->findby([], ['name' => 'ASC']);

        return $this->render('category/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/categorie/nouveau", name="add_category")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addCategorie(Request $request)
    {
        $newCategorie = new Category();
        $form = $this->createForm(CategoryType::class, $newCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newCategorie);
            $entityManager->flush();
            return $this->redirectToRoute('category');
        }
        return $this->render('category/newCategorie.html.twig', [
            'categorieForm' => $form->createView(),
        ]);
    }


    /**
     * @Route("/categorie/{id}", name="show_category")
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCategorie(Category $category,Request $request)
    {

        $response = $this->forward('App\Controller\ArticleController::fancy', [
            'name'  => $name,
            'color' => 'green',
        ]);

    }


    /**
     * @Route("/categorie/{id}/modifier", name="edit_category")
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editCategorie(Category $category)
    {

        return $this->render('category/editCategorie.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/categorie/{id}/supprimez", name="delete_category")
     * @param Category $category
     * @param ObjectManager $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteCategorie(Category $category, ObjectManager $em)
    {


       $em->remove($category);

       $em->flush();

        return $this->redirectToRoute('category');

    }
}