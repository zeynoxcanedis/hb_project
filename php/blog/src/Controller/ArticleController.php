<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="article_index", methods={"GET"})
     * @param Request $request
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function index(Request $request, ArticleRepository $articleRepository): Response
    {

        $filter = null;
        $selectCategory = null;
        $searched = null;
        $allArticles = $articleRepository->findall();
        if ($request->get('categorie') !== null || $request->get('recherche') !== null) {


            $articles = $articleRepository->searchArticle($request->get('recherche'), ($request->get('categorie')));
            $selectCategory = $request->get('categorie');
            $searched = $request->get('recherche');

        } else {
            $articles = $allArticles;
        }


        /** @var Category[] $categories */
        $categories = [];
        foreach ($allArticles as $article) {
            if (!in_array($article->getCategory(), $categories)) {
                $categories[] = $article->getCategory();
            }

        }
        return $this->render('article/index.html.twig', [
            'searched' => $searched,
            'selectCategory' => $selectCategory,
            'articles' => $articles,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/nouveau", name="article_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request): Response
    {
        /** @var Article $article */
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($article);
            $entityManager->flush();
            $article->setImageFile(null);
            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="article_show", methods={"GET"})
     * @param Article $article
     * @return Response
     */
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/{id}/modifier", name="article_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_index', [
                'id' => $article->getId(),
            ]);
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="article_delete", methods={"DELETE"})
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function delete(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete' . $article->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('article_index');
    }


    /**
     * @Route("/", name="search_categories")
     * @param ArticleRepository $articleRepository
     * @param request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchByCategory(ArticleRepository $articleRepository, Request $request)
    {


        return $this->render('article/index.html.twig', [
            'searchCategories' => $articleRepository

        ]);
    }


    /**
     * @Route("/upload", name="article_upload")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function upload(Request $request)
    {


        /** @var UploadedFile $files */
        $files = $request->files->get('upload');


        $target_dir = "images/upload/articles/";
        $target_file = $target_dir . basename($files->getClientOriginalName());
        $uploadOk = true;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image

        $check = getimagesize($files->getPathname());
        $uploadOk = ($check !== false);

// Check if file already exists
        if (file_exists($target_file)) {

            $uploadOk = false;
        }
// Check file size

// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {

            $uploadOk = false;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk) {
            $uploadOk = move_uploaded_file($files->getPathname(), $target_file);
        }

        $result = [
            'uploaded' => $uploadOk,
            'url' => $uploadOk ? $target_file : null
        ];
        if(!$uploadOk)
        {
            $result['error'] = [
                'message' => "prout" ];
        }
        return $this->json($result);
    }
}
