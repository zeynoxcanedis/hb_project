<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditUserPasswordType;
use App\Form\EditUserType;
use App\Form\RegistrationFormType;
use App\Security\AuthAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

class UserController extends AbstractController
{
    /**
     * @Route("/utilisateur", name="users")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);
        $users = $userRepository->findAll();


        return $this->render('user/index.html.twig', [
            "users" => $users
        ]);
    }

    /**
     * @Route("/utilisateur/{id}", name="show_user")
     */
    public function showUser(Request $request, int $id)
    {

        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);
        $user = $userRepository->find($id);


        return $this->render('\user\profil.html.twig', [
            'user' => $user,
        ]);
    }

    public static function loadValidatorData(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint(
            'oldPassword',
            new SecurityAssert\UserPassword([
                'message' => 'Wrong value for your current password',
            ])
        );
    }

    /**
     * @Route("/utilisateur/{id}/changer-mot-de-passe", name="edit_user_Password")
     * @param Request $request
     * @param int $id
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUserPassword(Request $request,int $id, UserPasswordEncoderInterface $passwordEncoder)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);
        $user = $userRepository->find($id);

        $form = $this->createForm(EditUserPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->get('plainPassword')->getData() !== null) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->render('user/editUserPassword.html.twig', [
            'editUserPasswordForm' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/utilisateur/{id}/modifier", name="edit_user")
     * @param Request $request
     * @param GuardAuthenticatorHandler $guardHandler
     * @param AuthAuthenticator $authenticator
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editUser(Request $request, GuardAuthenticatorHandler $guardHandler, AuthAuthenticator $authenticator, int $id)
    {

        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);
        $user = $userRepository->find($id);

        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUpdateAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $user->setImageFile(null);
            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }
        $user->setImageFile(null);

        return $this->render('user/editUser.html.twig', [
            'editUserForm' => $form->createView(),
            'user' => $user
        ]);
    }


    /**
     * @Route("/utilisateur/{id}/supprimez", name="delete_user")
     * @param Request $request
     * @param int $id
     * @param TokenStorageInterface $tokenStorage
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteUser(Request $request, int $id, TokenStorageInterface $tokenStorage)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);
        $user = $userRepository->find($id);
        /** @var  User $users */
        $actualUser = $this->getUser();

        $hasAccess = $this->isGranted('ROLE_ADMIN');

        if ($hasAccess || $user->getId() === $actualUser->getId()) {
            if ($user !== null) {
                $em->remove($user);
                $em->flush();
                $tokenStorage->setToken(null);
                $request->getSession()->invalidate();
            }

        } else {
            $this->addFlash(
                'acces denied',
                'Vous ne pouvez pas faire ça ! \(è_é\')/ '
            );
        }


        return $this->redirectToRoute('index');


    }
}