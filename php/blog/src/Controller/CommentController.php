<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/commentaire")
 */
class CommentController extends AbstractController
{

    /**
     * @Route("/{article}/nouveau" , name="comment_new", methods={"POST"})
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function new(Request $request, Article $article)
    {
        $comment = new Comment();

        $comment->setContent($request->get('comment'))
            ->setUser($this->getUser())
            ->setArticle($article);

        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        return $this->json([
            'username' => $this->getUser()->getUsername(),
            'content' => $comment->getContent(),
            'createdAt' => $comment->getCreatedAt()
        ]);
    }
}
