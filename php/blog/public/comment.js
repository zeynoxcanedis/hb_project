$(function () {
console.log('ceketuve');
    $("#commentForm").submit(function (event) {
        event.preventDefault();
        var comment = $("#comment").val();
        $.ajax({
            type: "POST",
            url: $('#article_container').data('path'),
            data: {comment: comment},
            success: function (data) {
                var formattedDate = new Date(data.createdAt);
                var d = formattedDate.getDate();
                var m =  formattedDate.getMonth();
                m += 1;  // JavaScript months are 0-11
                var y = formattedDate.getFullYear();

                $("#txtDate").val(d + "." + m + "." + y);
                $('#comment_list').append(
                    '<li>'+ data.username + ': ' + data.content +
                    ' (Crée le ' + d + "/" + m + "/" + y + ' )</li>');

                $("#comment").val(null);

            },
        });


    })
});