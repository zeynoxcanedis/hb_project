<?php
session_start();

$error = null;

if (isset($_POST['email']) && isset($_POST['password'])) {
    require_once 'database/user.php';
    $user = login($_POST['email'], $_POST['password']);
    if ($user !== null) {
        $_SESSION['user'] = $user;
    } else {
        $error = 'Identifiants invalides';
    }
}

if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
    header('Location: ./index.php');
    exit;
}

require_once 'header.php';
require_once 'navbar.php';
?>
    <div class="container">
        <h1 class="mt-3">Connexion</h1>

        <div class="py-md-3">
            <?php if ($error !== null) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
            <form method="POST">
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Mot de passe</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="Mot de passe">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Se connecter</button>
                    </div>
                </div>
            </form>
        </div>
        <p>Si vous n'avez pas de compte <a href="register.php">cliquez ici</a> pour vous inscrire !</p>

    </div>

<?php
require_once 'footer.php';