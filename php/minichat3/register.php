<?php

session_start();
require_once('./header.php');
require_once('./navbar.php');
require_once('./footer.php');

if (isset ($_POST['password2']) && isset($_POST['password'])) {
    if ($_POST['password2'] != $_POST['password']) {
        echo '<h1>Erreur</h1> <br> <p>Les mots de passe ne correspond pas..</p> <br> <a href="register.php">Réessayer</a> ';
        exit;
    }
}
if (isset($_POST['lastname']) && isset($_POST['firstname']) && isset($_POST['password']) && isset($_POST['email']) && isset($_POST['genre']) && isset($_POST['pseudo'])) {
    require_once('./database/user.php');
    $_POST['id'] = insertUser($_POST['firstname'], $_POST['lastname'], $_POST['email'],$_POST['password'], $_POST['genre'], $_POST['pseudo']);
    $_SESSION['user'] = $_POST;
}

if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
    header('Location: ./index.php');
    exit;
}


?>

<div class="container">
    <h1 class="mt-5">Inscription</h1>
    <div class="row d-flex">
        <div class="py-md-3 col-md-6">
            <form method="POST" action="register.php">
                <div class="form-group">
                    <label for="lastname">Nom</label>
                    <input type="text" class="form-control col-md-8" id="lastname" name="lastname"
                           placeholder="Votre nom">
                </div>
                <div class="form-group">
                    <label for="firstname">Prénom</label>
                    <input type="text" class="form-control col-md-8" id="firstname" name="firstname"
                           placeholder="Votre prenom">
                </div>
                <div class="form-group">
                    <label for="genre">Genre:</label>
                    <select class="form-control col-md-4" id="genre" name="genre">
                        <option>Homme</option>
                        <option>Femme</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">S'inscrire</button>
                </div>
        </div>
        <div class="py-md-3 col-md-6">
            <div class="form-group">
                <label for="email">Pseudo</label>
                <input type="text" class="form-control col-md-10" id="pseudo" name="pseudo"
                       placeholder="Pseudo">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control col-md-10" id="email" name="email" placeholder="Email">
            </div>

            <div class="form-group">
                <label for="password">Mot de passe</label>
                <input type="password" class="form-control col-md-8" id="password" name="password"
                       placeholder="Mot de passe">
            </div>
            <div class="form-group">
                <label for="password2">Confirmez votre mot de passe</label>
                <input type="password" class="form-control col-md-8" id="password2" name="password2"
                       placeholder="Mot de passe">
            </div>

            </form>
            <p>Si vous avez deja un compte <a href="login.php">cliquez ici</a> pour vous inscrire !</p>
        </div>
    </div>
</div>


