<?php
require_once 'header.php';
function getAllMessages(): ?array
{
    $db = new PDO('mysql:host=localhost;dbname=minichat;charset=utf8', 'root', '');

    $request = $db->prepare('SELECT * FROM message ORDER BY date_envoi ASC');
    $request->execute();
    return $request->fetchAll();
}

function addMessage($content, $userId)
{
    $date = date('Y-m-d H:i:s');

    $db = new PDO('mysql:host=localhost;dbname=minichat;charset=utf8', 'root', '');

    $request = $db->prepare('INSERT INTO message(contenu, id_utilisateur, date_envoi) VALUES (:contenu, :id_utilisateur, :date_envoi)');
    $request->execute(array(
        'contenu' => $content,
        'id_utilisateur' => $userId,
        'date_envoi' => $date
    ));
}


function btn_delete($message,$messageById)
{

    $btn = null;
    if ($message['id_utilisateur'] == $_SESSION['user']['id']) {
         $btn='<form action="./del.php" method="post"><button name="'. $messageById .'" class="btn btn-danger mx-1 my-2" value="'. $messageById .'">Supprimer</button></form></br>';
    }
    return $btn;
}

function btn_modify($message,$messageById)
{

    $btn = null;
    if ($message['id_utilisateur'] == $_SESSION['user']['id']) {
        $btn='<form action="./modify.php" method="post"><button name="'. $messageById .'" class="btn btn-warning mx-1 my-2" value="'. $messageById .'">Modifier</button></form>';
    }

    return $btn;
}




