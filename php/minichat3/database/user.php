<?php

function insertUser(string $firstname, string $lastname, string $email, string $password,string $genre, string $username): int
{
    $db = new PDO('mysql:host=localhost;dbname=minichat;charset=utf8', 'root', '');

    $request = $db ->prepare( 'INSERT INTO utilisateur(prenom,nom,email,mot_de_passe,genre,pseudo)  VALUES (:firstname, :lastname, :email, :psw, :genre, :username)');
    $request->execute(array(
        'firstname' => $firstname,
        'lastname' => $lastname,
        'email' => htmlspecialchars($email),
        'psw' => $password,
        'genre' => $genre,
        'username' => $username
    ));
    return $db->lastInsertId();
}
function login(string $email, string $password): ?array {
    $db = new PDO('mysql:host=localhost;dbname=minichat;charset=utf8', 'root', '');

    $request = $db->prepare('SELECT * FROM utilisateur WHERE email=:email AND mot_de_passe=:password');
    $request->execute(array(
        'email' => $email,
        'password' => $password
    ));

    $res = $request->fetchAll();
    return (count($res) === 1) ?  $res[0] : null;
}

function getUserById($id): ?array {
    $db = new PDO('mysql:host=localhost;dbname=minichat;charset=utf8', 'root', '');

    $request = $db->prepare('SELECT * FROM utilisateur WHERE id=:id');
    $request->execute(array(
        'id' => $id
    ));

    $res = $request->fetchAll();
    return (count($res) === 1) ? $res[0] : null;
}
