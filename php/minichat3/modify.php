<?php
session_start();
require_once 'header.php';
require_once 'navbar.php';


$db = new PDO('mysql:host=localhost;dbname=minichat;charset=utf8', 'root', '');
$id = implode($_SESSION['id']);
$request = $db->prepare('SELECT contenu FROM message WHERE id=:id');
$request->execute(array(
    'id' => $id
));
$content = $request->fetch();
$content2 = $content['contenu'];

if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
    unset($_SESSION['id']);
}

$_SESSION['id'] = $_POST;
unset($content);
?>
<div class="container">
    <form action="./messageEdit.php" method="post" class="mt-5 col-12 d-flex flex-column justify-center align-items-center ">
        <div class="form-group col-6">
            <textarea type="text" rows="4" cols="70" name="newContenu"><?php echo $content2 ?></textarea>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Envoyer">
        </div>
    </form>
</div>
