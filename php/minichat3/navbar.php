<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">Minichat</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>

    <?php if (isset($_SESSION['user']) && !empty($_SESSION['user'])){?>
            <li class="nav-item form-inline ">
                <a class="nav-link " href="./logout.php">Deconection</a>
            </li>
    <?php } ?>

    </div>
</nav>