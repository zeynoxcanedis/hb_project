<?php
session_start();


$error = null;


if (!isset($_SESSION['user']) || empty($_SESSION['user'])) {
    header('Location: ./login.php');
    exit;
}

require_once 'database/message.php';
require_once 'database/user.php';
require_once 'header.php';
require_once 'navbar.php';



if (isset($_POST['message']) && !empty($_POST['message'])) {
    addMessage($_POST['message'], $_SESSION['user']['id']);
}

$messages = getAllMessages();



?>
    <div class="container">
        <div class="py-md-3">
            Bienvenue sur ce super chat <strong><?php echo $_SESSION['user']['pseudo']; ?></strong>!
        </div>
        <?php

        ?>
        <ul >
            <?php foreach ($messages as $message):
                if ($message['id_utilisateur'] == $_SESSION['user']['id']) {
                    $user = $_SESSION['user'];
                    $color = "blue";
                } else {
                    $user = getUserById($message['id_utilisateur']);
                    switch ($user['id'] % 3) {
                        case 0:
                            $color = "green";
                            break;
                        case 1:
                            $color = "red";
                            break;
                        case 2:
                            $color = "purple";
                            break;
                        default:
                            $color = 'black';
                            break;
                    }
                }
                ?>
                <li class=" d-flex justify-between align-items-center align-" ><?php echo $message['date_envoi'] . ' <strong style="color:' . $color . ';"> &nbsp ' . $user['pseudo'] . ' </strong> : ' . $message['contenu'] . btn_modify($message,$message['id']). btn_delete($message,$message['id']) ?></li>

            <?php endforeach; ?>
        </ul>
        <form method="POST">
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" id="message" name="message" rows="3"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Envoyer">
            </div>
        </form>
    </div>

<?php

require_once 'footer.php';