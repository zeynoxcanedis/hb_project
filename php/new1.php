<?php
$varA = 1;
$varB = 2;

$varTemp = $varA;
$varA = $varB;
$varB = $varTemp;

echo "varA=" . $varA;
?>
<br>
<?php
echo "varB=" . $varB;
?>
<br>
<?php
echo "exercise 1.3";
?>
<br>
<?php
$varA = 4;
$varB = 20;
$varC = 6;
$varD = 13;
$varE = 0;

$varRes = ($varA + $varB + $varC + $varD + $varE) / 5;

echo "La moyenne est " . $varRes;
?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.1";
$varMoyenne = 0;
?>

<br>
<form action="new1.php" method="post">
    <p>La moyenne <input type="text" name="Moyenne"/></p>
    <p><input type="submit" value="OK"></p>
</form>
<?php
if (isset($_POST['Moyenne']) && !empty($_POST['Moyenne'])) {

    $varMoyenne = 0;
    $varRes = null;
    $varMoyenne = $_POST['Moyenne'];

    if ($varMoyenne >= 10):
        $varRes = "Réussi";
    elseif ($varMoyenne < 10 && $varMoyenne > 7):
        $varRes = "Rattrapage";
    else:
        $varRes = "Recalé";
    endif;
    echo $varRes;
}
$varRes = 0;

?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.2";
?>
<br>
<form action="new1.php" method="post">
    <p>Votre durée en seconde uniquement <input type="text" name="duree"/></p>
    <p><input type="submit" value="OK"></p>
</form>


<?php
if (isset($_POST['duree']) && !empty($_POST['duree'])) {
    $varHeure = null;
    $varMinute = null;
    $varSeconde = $_POST['duree'];
    $varSeconde2 = null;

    $varHeure = $varSeconde / 3600;
    $varMinute = ($varSeconde % 3600) / 60;
    $varSeconde2 = ($varSeconde % 3600) % 60;
    echo floor($varHeure) . " Heures, " . floor($varMinute) . " Minutes, " . floor($varSeconde2) . " Secondes";

}
?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.3";

?>
<br>
<form action="new1.php" method="post">
    <p>Le prix de l'article <input type="text" name="prix"/></p>
    <p><input type="submit" value="OK"></p>
</form>
<?php
if (isset($_POST['prix']) && !empty($_POST['prix'])) {
    session_start();
    if ($_POST['prix'] >= 100 && $_POST['prix'] < 500) {
        $_POST['prix'] = ($_POST['prix']) - (($_POST['prix'] * 5) / 100);
    } elseif ($_POST['prix'] >= 500) {
        $_POST['prix'] = ($_POST['prix']) - (($_POST['prix'] * 8) / 100);
    }
    $_SESSION['prixex23'] = $_SESSION['prixex23'] + $_POST['prix'];
} else {
    session_start();
    $_SESSION['prixex23'] = 0;
}
echo $_SESSION['prixex23'];

?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.4";
?>
<br>
<form action="new1.php" method="post">
    <p>Nombre de photocopis commander <input type="text" name="photocopieNB"/></p>
    <p><input type="submit" value="OK"></p>
</form>
<?php
if (isset($_POST['photocopieNB']) && !empty($_POST['photocopieNB'])) {
    if ($_POST['photocopieNB'] > 30) {
        $varPrix = (0.10 * 10) + (0.9 * 20) + (0.8 * ($_POST['photocopieNB'] - 30));
    } elseif ($_POST['photocopieNB'] > 10) {
        $varPrix = (0.10 * 10) + (0.9 * ($_POST['photocopieNB'] - 10));
    } else {
        $varPrix = 0.10 * $_POST['photocopieNB'];
    }
    echo $varPrix;
}
?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.5";
?>
<br>
<form action="new1.php" method="post">
    <p>Age <input type="text" name="age"/></p>
    <p><input type="submit" value="OK"></p>
</form>
<?php
if (isset($_POST['age']) && !empty($_POST['age'])) {
    if ($_POST['age'] === 6 || $_POST['age'] === 7) {
        $varRes = "Poussin";
    } elseif ($_POST['age'] === 8 || $_POST['age'] === 9) {
        $varRes = "Pupille";
    } elseif ($_POST['age'] === 10 || $_POST['age'] === 11) {
        $varRes = "Minime";
    } elseif ($_POST['age'] === 12) {
        $varRes = "Cadet";
    } else {
        $varRes = "suporter";
    }
    echo "L'enfant fait partie de la categorie " . $varRes;
}
?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.6";
?>
<br>
<br>
<?php
$retour = "</br>";
for ($i = 10; $i >= 0; $i--) {
    $varMes = $i;
    switch ($i) {
        case 2:
            echo " A vos marques ";
            break;
        case 1:
            echo "Prêt ";
            break;
        case 0:
            echo "Partez ! ";
            break;
        default:
            echo $i;
    }

    echo $retour;
}
?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.7";
?>
<br>
<form action="new1.php" method="post">
    <p>Nombre d'etoile a afficher <input type="text" name="nbEtoile"/></p>
    <p><input type="submit" value="OK"></p>
</form>
<?php

if (isset($_POST['nbEtoile']) && !empty($_POST['nbEtoile'])) {
    $varNB = $_POST['nbEtoile'];
    for ($i = $varNB; $i >= 1; $i--) {
        for ($j = $i; $j >= 1; $j--) {
            echo "*";
        }
        echo $retour;
    }
}
?>
<br>
<br>
<hr/>
<?php
echo "exercise 2.8";
?>
<br>
<form action="new1.php" method="post">
    <p>Nombre d'etoile a afficher <input type="text" name="nbEtoile2"/></p>
    <p><input type="submit" value="OK"></p>
</form>
<?php

if (isset($_POST['nbEtoile2']) && !empty($_POST['nbEtoile2'])) {
    $varNB = $_POST['nbEtoile2'];
    $varEtoile = 1;
    $varEspace = $varNB - $varEtoile;

    for ($i = 1; $i <= $varNB; $i++) {
        for ($j = 1; $j <= $varEspace; $j++) {
            echo "&nbsp;";
        }
        for ($k = 1; $k <= $varEtoile; $k++) {
            echo "*";
        }
        $varEtoile = $varEtoile + 2;
        $varEspace = $varEspace - 1;
        echo $retour;

    }
}
?>
<br>
<br>
<hr/>
<?php
echo "exercise 3.1";
?>
<br>
<?php
$table=[];
    for ($i = 1; $i <= 7; $i++){
    $table[]=0;
    }

    print_r($table);

?>
